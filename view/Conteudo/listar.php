<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="visualizacaoport" content="width=device-width, initial-scale=1">
 <title>Sistema de Pessoas - Home</title>

 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">

</head>

<?php
include '../../control/ConteudoControl.php';
$conteudoControl = new ConteudoControl();

header('Content-Type: application/json');

foreach($conteudoControl->findAll() as $valor){
	echo json_encode($valor);
}


?>

<body>

 

 <div id="main" class="container-fluid" style="margin-top: 50px">
 
 	<div id="top" class="row">
		<div class="col-sm-3">
			<h2>Pessoas</h2>
		</div>
		<div class="col-sm-5">
			
			<div class="input-group h2">
				<input name="data[search]" class="form-control" id="search" type="text" placeholder="Pesquisar Itens">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</span>
			</div>
			
		</div>
		<div class="col-sm-4">
			<a href="relatorio.html" class="btn btn-primary pull-right h2">Relatório</a>
		</div>
	</div> <!-- /#top -->
 
 
 	<hr />
 	<div id="list" class="row">
	
	<div class="table-responsive col-md-12">
		<table class="table table-striped" cellspacing="0" cellpadding="0">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Data Nascimento</th>
					<th>CPF</th>
					<th>Sexo</th>
					<th>Endereço</th>
					<th class="actions">Ações</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo 'nome';?></td>
					<td><?php echo 'dataNasc';?></td>
					<td><?php echo 'cpf';?></td>
					<td><?php echo 'sexo';?></td>
					<td><?php echo 'endereco';?></td>
					<td class="actions">
						<a class="btn btn-success btn-xs" href="listar.php">Visualizar</a>
						<a class="btn btn-warning btn-xs" href="editar.php">editar</a>
						<a class="btn btn-danger btn-xs"  href="excluir.php" data-toggle="modal" data-target="#delete-modal">Excluir</a>
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
	
	</div> <!-- /#list -->
	
 </div> <!-- /#main -->

<!-- Modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Excluir Item</h4>
      </div>
      <div class="modal-body">
        Deseja realmente excluir este item?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Sim</button>
	<button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
      </div>
    </div>
  </div>
</div>


 	<div class="row">
		<div class="col-sm-3">
				<a href="adicionar.html" class="btn btn-primary pull-right h2">Cadastrar Nova Pessoa</a>
		</div>

 <script src="js/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
</body>
</html>