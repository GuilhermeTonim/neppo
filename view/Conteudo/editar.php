<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>Sistema Pessoas - Editar</title>


 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">
</head>

<?php
include '../../control/ConteudoControl.php';
 
$data = file_get_contents('php://input');
$obj =  json_decode($data);
//echo $obj->titulo;

$id = $obj->id;


if(!empty($data)){	
 $conteudoControl = new ConteudoControl();
 $conteudoControl->update($obj , $id);
 header('Location:listar.php');
}


?>

<body>

 
 <div id="main" class="container-fluid">
  
  <h3 class="page-header">Editar Pessoa</h3>
  
  <form action="index.html">
  	<div class="row">
  	  <div class="form-group col-md-6">
  	  	<label for="nome">Nome</label>
  	  	<input type="text" class="form-control" id="nome">
  	  </div>
	  
	</div>
	
	<div class="row">
  	  <div class="form-group col-md-6">
  	  	<label for="dataNasc">Data Nascimento</label>
  	  	<input type="text" class="form-control" id="dataNasc">
  	  </div>
	 
	</div>
	
	<div class="row">
  	  <div class="form-group col-md-6">
  	  	<label for="CPF">CPF</label>
  	  	<input type="text" class="form-control" id="cpf">
  	  </div>
	  
	</div>
	
	<div class="row">
  	  <div class="form-group col-md-6">
  	  	<label for="sexo">Sexo</label>
  	  	<input type="text" class="form-control" id="sexo">
  	  </div>
	  
	</div>
	
	<div class="row">
  	  <div class="form-group col-md-6">
  	  	<label for="endereco">Endereço</label>
  	  	<input type="text" class="form-control" id="endereco">
  	  </div>
	  
	</div>
	
	<hr />
	
	<div class="row">
	  <div class="col-md-12">
	  	<button type="submit" class="btn btn-primary">Atualizar</button>
		<a href="index.html" class="btn btn-primary">Cancelar</a>
	  </div>
	</div>

  </form>
 </div>
 

 <script src="js/jquery.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
</body>
</html>