<!DOCTYPE html>
<html lang="pt-br">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <title>Sistema Pessoas - Adicionar</title>

 <link href="css/bootstrap.min.css" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet">

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 <script>
  var nome = null;
  var datanasc = null;
  var cpf = null;
  var sexo = null;
  var endereco = null;
  $(document).ready(function(){
    $.postJSON = function(url, data, callback) {
      return jQuery.ajax({
        'type': 'POST',
        'url': url,
        'contentType': 'application/json',
        'data': JSON.stringify(data),
        'dataType': 'json',
        'success': callback
      });
    };  

    $("#btnSalvar").click(function(){
      if (!$("#nome").val()) {
        window.alert("Informe o Nome");
      }
      else {
        if (!$("#datanasc").val()) {
          window.alert("Informe a Data Nascimento");
        }
        else {
          if (!$("#cpf").val()) {
            window.alert("Informe o CPF");
          }
          else {
            if (!$("#sexo").val()) {
              window.alert("Informe o Sexo");
            }
            else {
              if (!$("#endereco").val()) {
                window.alert("Informe o Endereço");
              }

            }
          }
        }
      }
    });
  });
</script>


</head>


<?php
include '../../control/ConteudoControl.php';
 
$data = file_get_contents('php://input');
$obj =  json_decode($data);
//echo $obj->titulo;



if(!empty($data)){	
 $conteudoControl = new ConteudoControl();
 $conteudoControl->insert($obj);
 header('Location:listar.php');
}


?>

<body>


 <div id="main" class="container-fluid">

  <h3 class="page-header">Adicionar</h3>
  
  <form action="index.html">
  	<div class="row">
     <div class="form-group col-md-5">
      <label for="nome">Nome: </label>
      <input type="text" class="form-control" id="nome">
    </div>

  </div>

  <div class="row">
   <div class="form-group col-md-5">
    <label for="datanasc">Data Nascimento</label>
    <input type="text" class="form-control" id="datanasc">
  </div>

</div>

<div class="row">
 <div class="form-group col-md-5">
  <label for="cpf">CPF</label>
  <input type="text" class="form-control" id="cpf">
</div>

</div>

<div class="row">
 <div class="form-group col-md-5">
  <label for="sexo">Sexo</label>
  <select class="form-control" id="sexo">
                          <option value="Masculino" checked>Masculino</option>
                          <option value="Feminino">Feminino</option>
                    </select> 
</div>

</div>

<div class="row">
 <div class="form-group col-md-5">
  <label for="endereco">Endereço</label>
  <input type="text" class="form-control" id="endereco">
</div>


</div>

<hr />

<div class="row">
 <div class="col-md-12">
  <button type="submit" id="btnSalvar" class="btn btn-primary">Salvar</button>
  <button type="reset" class="btn btn-primary">Limpar</button>
  <a href="listar.php" class="btn btn-primary">Voltar</a>
</div>
</div>

</form>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>