<?php
include '../../conexao/Conexao.php';

class Conteudo extends Conexao{
	private $nome;
    private $dataNasc;
    private $cpf;
    private $sexo;
    private $endereco;

    function getnome() {
        return $this->nome;
    }

    function getdataNasc() {
        return $this->dataNasc;
    }

    function getcpf() {
        return $this->cpf;
    }

    function getsexo() {
        return $this->sexo;
    }

    function getendereco() {
        return $this->endereco;
    }

    

    function setnome($nome) {
        $this->nome = $nome;
    }

    function setdataNasc($dataNasc) {
        $this->dataNasc = $dataNasc;
    }

    function setcpf($cpf) {
        $this->cpf = $cpf;
    }

    function setsexo($sexo) {
        $this->sexo = $sexo;
    }

    function setendereco($endereco) {
        $this->endereco = $endereco;
    }

   

    public function insert($obj){
    	$sql = "INSERT INTO pessoas (nome,dataNasc,cpf,sexo,endereco,disciplina_id) VALUES (:nome,:dataNasc,:cpf,:sexo,:endereco,:disciplina_id)";
    	$consulta = Conexao::prepare($sql);
        $consulta->bindValue('nome',  $obj->nome);
        $consulta->bindValue('dataNasc', $obj->dataNasc);
        $consulta->bindValue('cpf' , $obj->cpf);
        $consulta->bindValue('sexo' , $obj->sexo);
        $consulta->bindValue('endereco' , $obj->endereco);
    	return $consulta->execute();

	}

	public function update($obj,$id = null){
		$sql = "UPDATE pessoas SET nome = :nome, dataNasc = :dataNasc,cpf = :cpf, sexo = :sexo,endereco =:endereco WHERE id = :id ";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue('nome', $obj->nome);
		$consulta->bindValue('dataNasc', $obj->dataNasc);
		$consulta->bindValue('cpf' , $obj->cpf);
		$consulta->bindValue('sexo', $obj->sexo);
		$consulta->bindValue('endereco' , $obj->endereco);
		$consulta->bindValue('id', $id);
		return $consulta->execute();
	}

	public function delete($obj,$id = null){
		$sql =  "DELETE FROM pessoas WHERE id = :id";
		$consulta = Conexao::prepare($sql);
		$consulta->bindValue('id',$id);
		$consulta->execute();
	}

	public function find($id = null){

	}

	public function findAll(){
		$sql = "SELECT * FROM pessoas";
		$consulta = Conexao::prepare($sql);
		$consulta->execute();
		return $consulta->fetchAll();
	}

}

?>